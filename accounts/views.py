from django.shortcuts import redirect, render
from accounts.forms import login_form, signup_form
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout

# Create your views here.


def login_user(request):
    if request.method == "POST":
        form = login_form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = login_form()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def signup_user(request):
    if request.method == "POST":
        form = signup_form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            # create a new user with those values to save
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)

                return redirect("home")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = signup_form()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
