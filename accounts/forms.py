from django import forms
from receipts.models import Receipt


class login_form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())

    class Meta:
        model = Receipt


class signup_form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput()
    )
