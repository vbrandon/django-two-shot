from django import forms
from receipts.models import Receipt, Account, ExpenseCategory


class create_form(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


class Category_form(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class Account_form(forms.ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
